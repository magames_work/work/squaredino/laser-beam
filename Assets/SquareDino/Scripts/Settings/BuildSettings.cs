﻿using System;
using UnityEngine;

namespace SquareDino.Scripts.Settings
{
    [ExecuteInEditMode]
    public class BuildSettings : MonoBehaviour
    {
        private const string FLAG_REMOTECONFIG = "FLAG_REMOTE_CONFIG";
        private const string FLAG_FACEBOOK = "FLAG_FA";
        private const string FLAG_GAME_ANALYTICS = "FLAG_GA";
        private const string FLAG_APPS_FLYER = "FLAG_AF";
        private const string FLAG_FLURRY = "FLAG_FlURRY";
        private const string FLAG_TENJIN = "FLAG_TENJIN";
        private const string FLAG_IRON_SOURCE = "FLAG_IRONSOURCE";
        private const string FLAG_YANDEX_METRICA = "FLAG_YANDEX_METRICA";
        
        public bool remoteConfig;
        
        public bool iOs;
        public bool iOsFaceboook;
        public bool iOsGameAnalytics;
        public bool iOsAppsFlyer;
        public bool iOsFlurry;
        public bool iOsTenjin;
        public bool iOsIronSource;
        public bool iOsYandexMetrica;
        
        public bool android;
        public bool androidFaceboook;
        public bool androidGameAnalytics;
        public bool androidAppsFlyer;
        public bool androidFlurry;
        public bool androidTenjin;
        public bool androidIronSource;
        public bool androidYandexMetrica;

        private void Awake()
        {
            Load();
        }

        public string GenerateDefine_iOS(string value)
        {
            value = ClearOldDefines(value);
            if (value.LastIndexOf(';') != value.Length-1) value += ";";
            
            var newDefines = GenerateDefines_iOS();
            return value + newDefines;
        }
        
        public string GenerateDefine_Android(string value)
        {
            value = ClearOldDefines(value);
            if (value.LastIndexOf(';') != value.Length-1) value += ";";
            
            var newDefines = GenerateDefines_Android();
            return value + newDefines;
        }

        private string ClearOldDefines(string value)
        {
            value = RemoveKey(value, FLAG_REMOTECONFIG);
            value = RemoveKey(value, FLAG_FACEBOOK);
            value = RemoveKey(value, FLAG_GAME_ANALYTICS);
            value = RemoveKey(value, FLAG_APPS_FLYER);
            value = RemoveKey(value, FLAG_FLURRY);
            value = RemoveKey(value, FLAG_TENJIN);
            value = RemoveKey(value, FLAG_IRON_SOURCE);
            value = RemoveKey(value, FLAG_YANDEX_METRICA);
            return RemoveGarbage(value);
        }

        private string RemoveKey(string value, string key)
        {
            while (true)
            {
                var id = value.IndexOf(key, StringComparison.Ordinal);
                if (id <= -1) return value;

                value = value.Remove(id, key.Length);
            }
        }

        private string RemoveGarbage(string value)
        {
            var key = ";;";
            var id = value.IndexOf(key, StringComparison.Ordinal);
            if (id <= -1) return value;
            
            value = value.Remove(id, 1);
            return RemoveKey(value, key);
        }

        private string GenerateDefines_iOS()
        {
            var value = "";
            if (remoteConfig) value += FLAG_REMOTECONFIG + ";";
            if (iOsFaceboook) value += FLAG_FACEBOOK + ";";
            if (iOsGameAnalytics) value += FLAG_GAME_ANALYTICS + ";";
            if (iOsAppsFlyer) value += FLAG_APPS_FLYER + ";";
            if (iOsFlurry) value += FLAG_FLURRY + ";";
            if (iOsTenjin) value += FLAG_TENJIN + ";";
            if (iOsIronSource) value += FLAG_IRON_SOURCE + ";";
            if (iOsYandexMetrica) value += FLAG_YANDEX_METRICA + ";";
                
            return value;
        }
        
        private string GenerateDefines_Android()
        {
            var value = "";
            if (remoteConfig) value += FLAG_REMOTECONFIG + ";";
            if (androidFaceboook) value += FLAG_FACEBOOK + ";";
            if (androidGameAnalytics) value += FLAG_GAME_ANALYTICS + ";";
            if (androidAppsFlyer) value += FLAG_APPS_FLYER + ";";
            if (androidFlurry) value += FLAG_FLURRY + ";";
            if (androidTenjin) value += FLAG_TENJIN + ";";
            if (androidIronSource) value += FLAG_IRON_SOURCE + ";";
            if (androidYandexMetrica) value += FLAG_YANDEX_METRICA + ";";
                
            return value;
        }

        public void Save()
        {
            PlayerPrefs.SetInt("BuildSettings: remoteConfig", remoteConfig ? 1 : 0);
            
            PlayerPrefs.SetInt("BuildSettings: iOs", iOs ? 1 : 0);
            PlayerPrefs.SetInt("BuildSettings: iOsFaceboook", iOsFaceboook ? 1 : 0);
            PlayerPrefs.SetInt("BuildSettings: iOsGameAnalytics", iOsGameAnalytics ? 1 : 0);
            PlayerPrefs.SetInt("BuildSettings: iOsAppsFlyer", iOsAppsFlyer ? 1 : 0);
            PlayerPrefs.SetInt("BuildSettings: iOsFlurry", iOsFlurry ? 1 : 0);
            PlayerPrefs.SetInt("BuildSettings: iOsTenjin", iOsTenjin ? 1 : 0);
            PlayerPrefs.SetInt("BuildSettings: iOsIronSource", iOsIronSource ? 1 : 0);

            PlayerPrefs.SetInt("BuildSettings: android", android ? 1 : 0);
            PlayerPrefs.SetInt("BuildSettings: androidFaceboook", androidFaceboook ? 1 : 0);
            PlayerPrefs.SetInt("BuildSettings: androidGameAnalytics", androidGameAnalytics ? 1 : 0);
            PlayerPrefs.SetInt("BuildSettings: androidAppsFlyer", androidAppsFlyer ? 1 : 0);
            PlayerPrefs.SetInt("BuildSettings: androidFlurry", androidFlurry ? 1 : 0);
            PlayerPrefs.SetInt("BuildSettings: androidTenjin", androidTenjin ? 1 : 0);
            PlayerPrefs.SetInt("BuildSettings: androidIronSource", androidIronSource ? 1 : 0);
        }

        private void Load()
        {
            iOs = PlayerPrefs.GetInt("remoteConfig: remoteConfig") == 1;
            
            iOs = PlayerPrefs.GetInt("BuildSettings: iOs") == 1;
            iOsFaceboook = PlayerPrefs.GetInt("BuildSettings: iOsFaceboook") == 1;
            iOsGameAnalytics = PlayerPrefs.GetInt("BuildSettings: iOsGameAnalytics") == 1;
            iOsAppsFlyer = PlayerPrefs.GetInt("BuildSettings: iOsAppsFlyer") == 1;
            iOsFlurry = PlayerPrefs.GetInt("BuildSettings: iOsFlurry") == 1;
            iOsTenjin = PlayerPrefs.GetInt("BuildSettings: iOsTenjin") == 1;
            iOsIronSource = PlayerPrefs.GetInt("BuildSettings: iOsIronSource") == 1;

            android = PlayerPrefs.GetInt("BuildSettings: android") == 1;
            androidFaceboook = PlayerPrefs.GetInt("BuildSettings: androidFaceboook") == 1;
            androidGameAnalytics = PlayerPrefs.GetInt("BuildSettings: androidGameAnalytics") == 1;
            androidAppsFlyer = PlayerPrefs.GetInt("BuildSettings: androidAppsFlyer") == 1;
            androidFlurry = PlayerPrefs.GetInt("BuildSettings: androidFlurry") == 1;
            androidTenjin = PlayerPrefs.GetInt("BuildSettings: androidTenjin") == 1;
            androidIronSource = PlayerPrefs.GetInt("BuildSettings: androidIronSource") == 1;
        }
    }
}