using UnityEngine;

#if UNITY_IOS
    using UnityEngine.iOS;
#endif

namespace SquareDino.Scripts.Settings
{
    public class ProjectQuality : MonoBehaviour
    {
#if UNITY_IOS
    private void Start()
    {
        SetQualitySettings();

        Debug.Log("SystemInfo.deviceModel = " + SystemInfo.deviceModel);
    }

    private void SetQualitySettings()
    {
        if (Device.generation <= DeviceGeneration.iPhone6
            || Device.generation <= DeviceGeneration.iPadMini4Gen
            || Device.generation <= DeviceGeneration.iPad5Gen
            || Device.generation <= DeviceGeneration.iPadAir2
        )
            QualitySettings.SetQualityLevel(0);
    }
#endif
    }
}