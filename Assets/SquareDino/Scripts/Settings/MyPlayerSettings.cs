﻿using UnityEngine;

namespace SquareDino.Scripts.Settings
{
    public class MyPlayerSettings : MonoBehaviour
    {
        public Texture2D icon;
        public string gameName = "Test project";
        public string bundleVersion = "0.0.1";
        public string applicationIdentifier = "";
        public Sprite logo;
    }
}