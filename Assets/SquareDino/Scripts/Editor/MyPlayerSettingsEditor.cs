﻿namespace SquareDino.Editor
{
    using Scripts.Settings;
    using UnityEditor;
    using UnityEngine;
    
    [CustomEditor(typeof(MyPlayerSettings))]
    public class MyPlayerSettingsEditor : Editor
    {
        public override void OnInspectorGUI()
        {
            base.OnInspectorGUI();
            
            var settings = target as MyPlayerSettings;
            if (settings == null) return;

            if (!GUILayout.Button("Apply Settings")) return;
            
            Texture2D[] icon = {settings.icon};
            PlayerSettings.SetIconsForTargetGroup(BuildTargetGroup.Unknown, icon);
            PlayerSettings.companyName = "SquareDino";
            PlayerSettings.productName = settings.gameName;
            PlayerSettings.bundleVersion = settings.bundleVersion;
            if (settings.applicationIdentifier != "")
                PlayerSettings.applicationIdentifier = settings.applicationIdentifier;
                
            PlayerSettings.SplashScreen.show = true;
            PlayerSettings.SplashScreen.showUnityLogo = false;
            PlayerSettings.SplashScreen.unityLogoStyle = PlayerSettings.SplashScreen.UnityLogoStyle.DarkOnLight;
            var logos = new PlayerSettings.SplashScreenLogo[1];
            var splashScreenLogo = PlayerSettings.SplashScreenLogo.Create(2f, settings.logo);
            logos[0] = splashScreenLogo;
            PlayerSettings.SplashScreen.logos = logos;
            PlayerSettings.allowedAutorotateToPortrait = true;
            PlayerSettings.allowedAutorotateToLandscapeLeft = false;
            PlayerSettings.allowedAutorotateToLandscapeRight = false;
            PlayerSettings.allowedAutorotateToPortraitUpsideDown = false;
            PlayerSettings.defaultInterfaceOrientation = UIOrientation.Portrait;
            // Screen.orientation = ScreenOrientation.Landscape;
        }
    }
}
