﻿using SquareDino.Scripts.Settings;
using UnityEditor;
using UnityEngine;

namespace SquareDino.Scripts.Editor
{
    [CustomEditor(typeof(BuildSettings))]
    public class BuildSettingsEditor : UnityEditor.Editor
    {
        public override void OnInspectorGUI()
        {
            var settings = target as BuildSettings;
            if (settings == null) return;
            
            var somethingChanged = false;

            somethingChanged |= Toggle("Remote Config", ref settings.remoteConfig);
            
            settings.iOs = EditorGUILayout.BeginToggleGroup("AppStore SDKs", settings.iOs);
            if (settings.iOs)
            {
                somethingChanged |= Toggle("Facebook", ref settings.iOsFaceboook);
                somethingChanged |= Toggle("GameAnalytics", ref settings.iOsGameAnalytics);
                somethingChanged |= Toggle("AppsFlyer", ref settings.iOsAppsFlyer);
                somethingChanged |= Toggle("Flurry", ref settings.iOsFlurry);
                somethingChanged |= Toggle("Tenjin", ref settings.iOsTenjin);
                somethingChanged |= Toggle("IronSource", ref settings.iOsIronSource);
                somethingChanged |= Toggle("YandexMetrica", ref settings.iOsYandexMetrica);
            }

            EditorGUILayout.EndToggleGroup();
            if (somethingChanged)
            {
                var oldDefine = PlayerSettings.GetScriptingDefineSymbolsForGroup(BuildTargetGroup.iOS);
                var define = settings.GenerateDefine_iOS(oldDefine);
                PlayerSettings.SetScriptingDefineSymbolsForGroup(BuildTargetGroup.iOS, define);
            }
            
            
            somethingChanged = false;
            settings.android = EditorGUILayout.BeginToggleGroup("GooglePlay SDKs", settings.android);
            if (settings.android)
            {
                somethingChanged |= Toggle("Facebook", ref settings.androidFaceboook);
                somethingChanged |= Toggle("GameAnalytics", ref settings.androidGameAnalytics);
                somethingChanged |= Toggle("AppsFlyer", ref settings.androidAppsFlyer);
                somethingChanged |= Toggle("Flurry", ref settings.androidFlurry);
                somethingChanged |= Toggle("Tenjin", ref settings.androidTenjin);
                somethingChanged |= Toggle("IronSource", ref settings.androidIronSource);
                somethingChanged |= Toggle("YandexMetrica", ref settings.androidYandexMetrica);
            } 
            
            EditorGUILayout.EndToggleGroup();

            if (somethingChanged)
            {
                var oldDefine = PlayerSettings.GetScriptingDefineSymbolsForGroup(BuildTargetGroup.Android);
                var define = settings.GenerateDefine_Android(oldDefine);
                PlayerSettings.SetScriptingDefineSymbolsForGroup(BuildTargetGroup.Android, define);
            }
            
            if (GUI.changed)
            {
                EditorUtility.SetDirty(target);
            }
            
            settings.Save();
        }

        private bool Toggle(string text, ref bool value)
        {
            var prevValue = value;
            value = EditorGUILayout.Toggle(text, value);
            return prevValue != value;
        }
    }
}