namespace SquareDino.Editor
{
    using Scripts.Settings;
    using UnityEditor;
    using UnityEngine;
    
    [CustomEditor(typeof(MyQualitySettings))]
    public class MyQialitySettingsEditor : Editor
    {
        public override void OnInspectorGUI()
        {
            base.OnInspectorGUI();
            
            var settings = target as MyQualitySettings;
            if (settings == null) return;
            
            if (GUILayout.Button("Apply Settings"))
            {
                settings.ApplySettings();
            }
        }
    }
}