﻿using UnityEngine;

#if !UNITY_EDITOR&&FLAG_GA
	using GameAnalyticsSDK;
#endif

namespace SquareDino.Scripts.CurrentAPIs
{
	public class MyGameAnalytics : MonoBehaviour
	{
		private void Start()
		{
#if !UNITY_EDITOR&&FLAG_GA
			GameAnalytics.Initialize();
#endif
		}
	}
}