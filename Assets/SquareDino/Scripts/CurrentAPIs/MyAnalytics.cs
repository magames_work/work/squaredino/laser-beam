﻿using UnityEngine;

#if !UNITY_EDITOR
        using System.Collections.Generic;
        using UnityEngine.Analytics;

        #if FLAG_FA
                using Facebook.Unity;
        #endif

        #if FLAG_GA
                using GameAnalyticsSDK;
        #endif
#endif

namespace SquareDino.Scripts.CurrentAPIs
{
    public class MyAnalytics : MonoBehaviour
    {
        public static void LevelStart(int currentLevel)
        {
                #if !UNITY_EDITOR
                        Analytics.CustomEvent("Level Start", new Dictionary<string, object> {{"Id", currentLevel}});
                                
                        #if FLAG_FA
                                FB.LogAppEvent ("game_start");
                        #endif
                                        
                        #if FLAG_GA
                                GameAnalytics.NewProgressionEvent(GAProgressionStatus.Start, "Level " + currentLevel);
                        #endif
                
                        #if FLAG_YANDEX_METRICA
                                AppMetrica.Instance.ReportEvent("level_start", new Dictionary<string, object> {{"level", currentLevel}});
                                AppMetrica.Instance.SendEventsBuffer();
                        #endif
                #endif
                
        }

        public static void LevelFailed(int currentLevel, int score = 0)
        {
                #if !UNITY_EDITOR
                        Analytics.CustomEvent("Level Failed", new Dictionary<string, object> {{"Id", currentLevel}});
                
                        #if FLAG_FA
                                FB.LogAppEvent ("game_end");
                                //FB.LogAppEvent ("game_end_levels");
                        #endif
                                        
                        #if FLAG_GA
                                GameAnalytics.NewProgressionEvent(GAProgressionStatus.Fail, "Level" + currentLevel, score);
                        #endif
                        #if FLAG_AF
                                var eventName = "af_fakeImpression";
                                Dictionary<string, string> eventParams = new Dictionary<string, string>(){{"imp","1"}}; AppsFlyer.trackRichEvent(eventName, eventParams);
                        #endif
                        #if FLAG_YANDEX_METRICA
                                AppMetrica.Instance.ReportEvent("level_finish",
                                        new Dictionary<string, object> {{"result", "lose"},
                                                {"time", 0},
                                                {"progress", 0}});
                                AppMetrica.Instance.SendEventsBuffer();
                        #endif
                #endif
                
        }

        public static void LevelWin(int currentLevel, int score = 0)
        {
                #if !UNITY_EDITOR
                        Analytics.CustomEvent("Level Finished", new Dictionary<string, object> {{"Id", currentLevel}});
                        
                        
                        #if FLAG_FA
                                FB.LogAppEvent ("game_end");
                        #endif
                                        
                        #if FLAG_GA
                                GameAnalytics.NewProgressionEvent(GAProgressionStatus.Complete, "Level" + currentLevel, score);
                        #endif
                
                        #if FLAG_AF
                                var eventName = "af_fakeImpression";
                                Dictionary<string, string> eventParams = new Dictionary<string, string>(){{"imp","1"}}; AppsFlyer.trackRichEvent(eventName, eventParams);
                        #endif
                
                        #if FLAG_YANDEX_METRICA
                                AppMetrica.Instance.ReportEvent("level_finish",
                                        new Dictionary<string, object> {{"result", "win"},
                                                {"time", 1},
                                                {"progress", 100}});
                        #endif
                #endif
        }
    
        public static void OnAdsShowInterstitial()
        {
                #if !UNITY_EDITOR

                        #if FLAG_FA
                                FB.LogAppEvent ("ad_shown_interstitial");
                        #endif
                                        
                        #if FLAG_GA

                        #endif
                #endif
        }
    
        public static void OnAdsShowBanner()
        {
                #if !UNITY_EDITOR
                        #if FLAG_FA
                                FB.LogAppEvent ("banner_shown");
                        #endif
                                
                        #if FLAG_GA

                        #endif
                #endif
        }
    
        public static void OnAdsShowRewarded(bool finished)
        {
                #if !UNITY_EDITOR
                        #if FLAG_FA
                                FB.LogAppEvent ("rewarded_shown: " + finished);
                        #endif
                 #endif
        }

        public static void NewHighScore(int value)
        {
//#if !UNITY_EDITOR
//        Analytics.CustomEvent("HighScore", new Dictionary<string, object> {{"sessions", PrefsManager.GameplayCounter}, {"score", value}});
//#endif
        }

        public static void RateClick()
        {
//#if !UNITY_EDITOR
//        Analytics.CustomEvent("RateFeedbackClick",
//            new Dictionary<string, object> {{"sessions", PrefsManager.GameplayCounter}});
//#endif
        }
    }
}