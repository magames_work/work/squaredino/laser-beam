﻿using UnityEngine;
#if !UNITY_EDITOR && FLAG_FA
    using Facebook.Unity;
#endif

namespace SquareDino.Scripts.CurrentAPIs
{
    public class MyFacebookAnalytics : MonoBehaviour
    {
        #if !UNITY_EDITOR && FLAG_FA
        private void Awake()
        {
            if (!FB.IsInitialized)
            {
                FB.Init(InitCallback);
            }
            else
            {
                FB.ActivateApp();
            }
        }

        private void InitCallback()
        {
            if (FB.IsInitialized)
            {
                // Signal an app activation App Event
                FB.ActivateApp();
                Debug.Log("Facebook SDK - Initialized");
                // Continue with Facebook SDK
                // ...
            }
            else
            {
                Debug.Log("Failed to Initialize the Facebook SDK");
            }
        }
        #endif
    }
}