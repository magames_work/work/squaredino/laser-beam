﻿using System;
// using DG.Tweening;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace SquareDino.Scripts.Utils
{
    public class Hint : MonoBehaviour
    {
        [SerializeField] private TextMeshProUGUI text;
        [SerializeField] private Image handImage;
        [SerializeField] private Transform targetPoint;
        // private Tweener textTweener;
        // private Sequence _sequence;

        private void Start()
        {

            // if (PlayerPrefs.GetInt("currentLevelID") > 0)
            // {
            //     GlobalEvents<OnGameInputEnable>.Call(new OnGameInputEnable {Flag = false});
            //     Destroy(gameObject);
            //     return;
            // }
            // GlobalEvents<OnGameInputEnable>.Call(new OnGameInputEnable {Flag = true});
            // textTweener = text.DOColor(text.color * new Color(1f, 1f, 1f, .5f), .5f).SetLoops(-1, LoopType.Yoyo);
            //
            // _sequence = DOTween.Sequence();
            // var tween = handImage.transform.DOScale(Vector3.one * 1.4f, .0f);
            // _sequence.Append(tween);
            // tween = handImage.transform.DOScale(Vector3.one, .3f);
            // _sequence.Append(tween);
            // tween = handImage.transform.DOMove(targetPoint.position, .8f);
            // _sequence.Append(tween);
            // _sequence.AppendInterval(.25f);
            // _sequence.SetLoops(-1, LoopType.Restart);
            // _sequence.Play();
        }

        private void OnEnable()
        {
            GameInput.OnPointerUp += OnPointerUp;
        }
        
        private void OnDisable()
        {
            GameInput.OnPointerUp -= OnPointerUp;
        }

        private void OnPointerUp()
        {
            // textTweener?.Kill();
            // _sequence?.Kill();
            // GlobalEvents<OnGameInputEnable>.Call(new OnGameInputEnable{Flag = false});
            // Destroy(gameObject);
        }
    }
}