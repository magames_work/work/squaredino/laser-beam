using System.Collections.Generic;
using UnityEngine;

namespace SquareDino.Scripts.Utils
{
    public class RandomNoRepeate
    {
        private readonly List<int> _available = new List<int>();
        private int _count;

        public void SetCount(int value)
        {
            _count = value;
            Init();
        }

        public int GetAvailable()
        {
            CheckAvailableIds();

            var availableId = Random.Range(0, _available.Count);
            var id = _available[availableId];
            _available.RemoveAt(availableId);

            return id;
        }

        public int GetAvailableAtId(int availableId)
        {
            if (availableId < 0 || availableId >= _count) return -1;

            CheckAvailableIds();

            if (availableId < _available.Count)
                SetCount(_count);

            var id = _available[availableId];
            _available.RemoveAt(availableId);

            return id;
        }

        private void CheckAvailableIds()
        {
            if (_available.Count == 0) Init();
        }

        public void Init()
        {
            _available.Clear();
            for (var i = 0; i < _count; i++) _available.Add(i);
        }
    }
}