﻿using System;
using UnityEngine;
using UnityEngine.UI;

namespace SquareDino.Scripts.Utils
{
	public class GameInput : MonoBehaviour
	{
		public static event Action OnPointerDown = delegate{};  
		public static event Action OnPointerUp = delegate{};  
		public static event Action OnPointerMove = delegate{};  
		private Image _image;

		private void OnEnable ()
		{
			_image = GetComponent<Image>();
			GlobalEvents<OnGameInputEnable>.Happened += OnGameInputEnable;
		}

		private void OnGameInputEnable(OnGameInputEnable obj)
		{
			_image.raycastTarget = obj.Flag;
		}
	
		public void PointerDown()
		{
			OnPointerDown();
		}
	
		public void PointerUp()
		{
			OnPointerUp();
		}
	
		public void PointerMove()
		{
			OnPointerMove();
		}
	}
}