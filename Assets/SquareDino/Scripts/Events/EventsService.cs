﻿using UnityEngine;

public struct OnGameStart{}
public struct OnPlayerRun{}
public struct OnGameLevelWin{}

public struct OnGameOverPrepare
{
    public bool IsHumanPlayer;
}
public struct OnGameOver {
    public int totalGames;
}
public struct OnGameReady {}

public struct OnLevelProgressSet
{
    public int value;
}

public struct OnGameReaction
{
    public bool Perfect;
}
public struct OnGameReactionFlash
{
    public Color Color;
}
public struct OnGameLoaded { }

public struct OnShowMenu{}
public struct OnHideMenu{}

public struct OnScreenReviveShow{}

public struct OnScreenSkinsShow{}
public struct OnScreenSkinsHide{}

public struct OnScreenRateShow
{
    public bool IsBtnClick;
}

public struct OnScreenLeaderboardShow{}
public struct OnLeaderboardAddInfo
{
    public int Id;
    public float Damage;
}

public struct OnLeaderboardAddItem
{
    public int Id;
    public bool IsPlayer;
}

public struct OnVibrate
{
    public int TrySmooth;
    public bool OnlySmooth;
}

public struct OnMissIndicator
{
    public Vector2 Position;
}

public struct OnRate{}

public struct OnCameraZoom
{
    public float ZoomValue;
}

public struct OnCameraOffsetAdd
{
    public Vector3 Offset;
}

public struct OnPointsAdd
{
    public bool Perfect;
}
public struct OnPointsAddAtPosition
{
    public bool Perfect;
    public Vector3 Position;
}

public struct OnComboReset{}
public struct OnCombo
{
    public int Count;
}

// Сросить идикатор очков
public struct OnPointsPopupAdd
{
    public int Count;
}

// Сросить идикатор очков
public struct OnPointsPopupAddAtPosition
{
    public int Count;
    public Vector3 Position;
}

//--------------------------------------------------------
// Game Input - объект, который принимает клики по игровому
// полю и посылает их в игру
//--------------------------------------------------------

public struct OnGameInputEnable
{
    public bool Flag;
}

//--------------------------------------------------------
// BestScore
//--------------------------------------------------------
public struct OnBestScoreNew
{
    public int Count;
}

//--------------------------------------------------------
// TIMER PROGRESS BAR
//--------------------------------------------------------
// Показать Таймер
public struct OnTimerShow {}
// Добавить время
public struct OnTimerRivive{}
// Старт таймера
public struct OnTimerStart
{
    public float TimeCount;
}
// Продолжить Таймер
public struct OnTimerContinue{}
// Пауза
public struct OnTimerPause{}
// Ресет
public struct OnTimerReset{}
public struct OnTimerHide{}




//--------------------------------------------------------
// ADS
//--------------------------------------------------------
// Отключить рекламу
public struct OnAdsDisable {}
public struct OnAdsVideoShow {}
public struct OnAdsVideoClosed {}
public struct OnAdsRewardedShow {}
public struct OnAdsRewardedClosed
{
    public bool IsReward;
}

public struct OnAdsBannerShow {}
public struct OnAdsBannerHide {}

// Можно дарить подарок
public struct OnGiftAvailable
{
    public bool IsAvailable;
}

// Добавляем монетки
public struct OnCoinsAdd
{
    public int Count;
}

// Добавили монеток
public struct OnCoinsAdded
{
    public int Total;
}

// Смотрим видео и получаем скин
public struct OnAdsRewardedBuySkin
{
    public int Id;
}

// Купили скин за просмотр видео
public struct OnBuySkinByRewarded
{
    public int Id;
}

//--------------------------------------------------------
// BUTTONS CLICKS
//--------------------------------------------------------

// Нажали на кнопку "Поделиться игрой"
public struct OnBtnShareClick{}

// Нажали на кнопку "Поделиться Gif"
public struct OnBtnShareGifClick
{
}

// Показать на экран Скин, который игрок получает после нажатия на ленточку "Получить скин за 200 монет"
public struct OnBuySkin
{
    public int Id;
}

// Купили скин за реальные деньги
public struct OnBuySkinByIAP
{
    public int Id;
}

// Разблокировать все скины и отключить рекламу
public struct OnSkinsUnlockAll{}

// Показать на экран Скин, который игрок получает после нажатия на ленточку "Получить скин"
public struct OnGiftSkin
{
    public int Id;
}

// Все скины открыты
public struct OnSkinAllOpened{}

// Все GENERAL скины открыты
public struct OnSkinAllGeneralOpened{}

//--------------------------------------------------------
//						    Gift
//--------------------------------------------------------

// Высыпать на экран горсть монет
public struct OnCoinsAddToScreen
{
    public int CoinsCount;
}

// Закончилась анимация Вручения подарка
public struct OnGiftCollected{}

// Показать на экран Скин, который игрок получает после нажатия на ленточку "Получить скин за 200 монет"
public struct OnGiftShowRandomSkinAnimation{}

// Скрыть экран подарка
public struct OnHideGiftScreen{}

// Закончили проигрывание анимации подарка
public struct OnGiftAnimationDone{}

// Высыпать на экран горсть монет
public struct OnGiftResetTimer
{
    public bool IsResetTimer;
}

//--------------------------------------------------------
//							GIF
//--------------------------------------------------------

// На финишном эране нет кнопок для показа
public struct OnGifSaved{}

// Закончилась анимация Вручения подарка
public struct OnGifShared{}

//--------------------------------------------------------
//							IAPs
//--------------------------------------------------------
public struct OnIAPsBuySkin
{
    public int Id;
}

public struct OnIAPsBuyTier1{}

public struct OnIAPsBuyTier2{}

public struct OnIAPsBuyNoAds{}

public struct OnIAPsBuyUnlockAll{}

//--------------------------------------------------------
//							Achievements
//--------------------------------------------------------
public struct AchievementProgress
{
    public string Id;
    public int Progress;
}

//--------------------------------------------------------
//							Debug
//--------------------------------------------------------

public struct OnDebugLog
{
    public string message;
}


