﻿namespace SquareDino
{
    public struct OnAdsDisable {}
    public struct OnAdsVideoShow {}
    public struct OnAdsVideoClosed {}
    public struct OnAdsRewardedShow {}
    public struct OnAdsRewardedClosed
    {
        public bool IsReward;
    }

    public struct OnAdsBannerShow {}
    public struct OnAdsBannerHide {}
}