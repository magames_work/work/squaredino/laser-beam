﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LVLManager : MonoBehaviour
{
//Lists
    public List<Lvls> Levels = new List<Lvls>();
    [Serializable]
    public class Lvls
    {
        public GameObject lvlPrefab;
        public float PointsNeed;
    }

//Classes
    
    [HideInInspector]
    public int nextLVL;
    [HideInInspector]
    public int currentlvl;
    [HideInInspector]
    public int currentlvlUI;
    [HideInInspector]
    public int nextlvlUI;
    [HideInInspector]
    public bool Debug;
    
    //config
    public float PointsCurrent;
    public int LvlAfterEnd;
    private int CountNextLVL;
    private GameObject currentObj, NextObj;
    public float AmountEnergy;
    public float ReceiveEnergy;
    public int Stars;
    public Game_Manager gm;
    private int prevLvl;
    private int CannonsCount;
    public int CannonsOutOfEnergy;
    public bool isFinished;
    public bool Finish;
    private bool isFailed;

    private void Start()
    {
        if (!PlayerPrefs.HasKey("Saved LVL")) currentlvl = 0;
        else currentlvl = PlayerPrefs.GetInt("Saved LVL");
        if (!PlayerPrefs.HasKey("Saved LVL UI")) currentlvlUI = currentlvl;
        else currentlvlUI = PlayerPrefs.GetInt("Saved LVL UI");

        CalculateLvls();
        gm.UIManager.LvlCountUpdate();
        AmountEnergy = gm.EnergyTotal;
        

        currentObj = Instantiate(Levels[currentlvl].lvlPrefab, new Vector3(0, 0, 0), Levels[currentlvl].lvlPrefab.transform.rotation) as GameObject;
        //var cannonscount = FindObjectsOfType<Laser>();
        //CannonsCount = cannonscount.Length;
        //gm.UIManager.SliderCount(CannonsCount);
        CalculatePointNeed();
        gm.ShopManager.StartChangeSkin();
       // gm.UIManager.fadeout();
    }
    private void Update()
    {
        CalculateLvls();

        if(gm.EnergyTotal <= 0 | ReceiveEnergy >= Levels[currentlvl].PointsNeed)
        {
            isFinished = true;
            Finish = true;
            gm.EnergyTotal = 1;
        }

        if(ReceiveEnergy >= Levels[currentlvl].PointsNeed * 0.6f & ReceiveEnergy < Levels[currentlvl].PointsNeed * 0.75f)
        {
            GameObject finishObj = GameObject.FindGameObjectWithTag("finish");
            Material[] mats = new Material[2];
            mats.SetValue(finishObj.GetComponent<MeshRenderer>().materials[0], 0);
            mats.SetValue(gm.LightOn, 1);
            finishObj.GetComponent<MeshRenderer>().materials = mats;

            
        }

        if (isFinished & !hvatitPlz)
        { 
            if(ReceiveEnergy < Levels[currentlvl].PointsNeed * 0.6f)
            {
                Stars = 0;
            }
            if(ReceiveEnergy >= Levels[currentlvl].PointsNeed * 0.6f & ReceiveEnergy < Levels[currentlvl].PointsNeed * 0.75f)
            {
                Stars = 1;
            }
            if(ReceiveEnergy >= Levels[currentlvl].PointsNeed * 0.75f & ReceiveEnergy < Levels[currentlvl].PointsNeed * 0.9f)
            {
                Stars = 2;
            }
            if(ReceiveEnergy >= Levels[currentlvl].PointsNeed * 0.9f)
            {
                Stars = 3;

                GameObject finishObj = GameObject.FindGameObjectWithTag("finish");
                gm.halo.SetActive(true);
                gm.halo.transform.position = new Vector3(finishObj.transform.position.x, finishObj.transform.position.y, gm.halo.transform.position.z);
            }
            ReceiveEnergy = 0;

            if(Stars >= 1)
            {
                
                NextLvl();
                print(Stars);
                isFinished = false;
            }
            else
            {
                //print(Stars);
                lvlFailed();
                isFinished = false;
            }
            print(Stars);
        }

    }

    void lvlFailed()
    {
        if (!isFailed)
        {
            gm.UIManager.LvlFailed();
            gm.VibroManager.TriggerFailure();
            
            PointsCurrent = -1;
            isFailed = true;
        }
    }

    bool hvatitPlz;
    public void NextLvl()
    {
        if(!gm.UIManager.skipCollectStars)
        {
            gm.startsTotal += Stars;
        }
        AmountEnergy = 0;
        ReceiveEnergy = -1;
        prevLvl = currentlvl;
        currentlvl++;
        currentlvlUI++;
        PlayerPrefs.SetInt("Saved LVL", currentlvl);
        PlayerPrefs.SetInt("Stars", gm.startsTotal);
        PlayerPrefs.SetInt("Saved LVL UI", currentlvlUI);
        gm.VibroManager.TriggerSuccess();
        gm.UIManager.LvlCleared();
        PointsCurrent = -1;
        CannonsOutOfEnergy = 0;
        CannonsCount = 1;
        gm.EnergyTotal = 10000;
        hvatitPlz = true;
    }

    public void RestartLvl() 
    { 
        gm.UIManager.fadeout();
        Finish = false;
        hvatitPlz = false;
        isFinished = false;
        //AmountEnergy = 0;
        ReceiveEnergy = 0;
        PointsCurrent = 0;
        CannonsOutOfEnergy = 0;
        DestroyImmediate(currentObj);
        currentObj = Instantiate(Levels[currentlvl].lvlPrefab, new Vector3(0, 0, 0), Levels[currentlvl].lvlPrefab.transform.rotation) as GameObject;
        gm.UIManager.ResetProgressBar();
        CalculatePointNeed();
        //CalculateCannonsCount();
        gm.UIManager.ShopPanel.gameObject.SetActive(true);
        gm.ShopManager.ChangeSkinWithoutItem();
        gm.EnergyTotal = 160;
        gm.UIManager.BigEnergySlider.gameObject.SetActive(true);
        gm.UIManager.fillBigSlider();
        isFailed = false;
    }
    public void RestartLvl2() 
    { 
        gm.UIManager.fadeout();
        Finish = false;
        hvatitPlz = false;
        isFinished = false;
        //AmountEnergy = 0;
        ReceiveEnergy = 0;
        PointsCurrent = 0;
        CannonsOutOfEnergy = 0;
        DestroyImmediate(currentObj);
        if(currentlvl - 1 < 0)
        {
            currentlvl = Levels.Count - 1;
            currentlvlUI = currentlvlUI - 1;
            PlayerPrefs.SetInt("Saved LVL", currentlvl);
            PlayerPrefs.SetInt("Saved LVL UI", currentlvlUI);
        }
        else
        {
            currentlvl = currentlvl - 1;
            currentlvlUI = currentlvlUI - 1;
            PlayerPrefs.SetInt("Saved LVL", currentlvl);
            PlayerPrefs.SetInt("Saved LVL UI", currentlvlUI);
        }
        currentObj = Instantiate(Levels[currentlvl].lvlPrefab, new Vector3(0, 0, 0), Levels[currentlvl].lvlPrefab.transform.rotation) as GameObject;
        gm.UIManager.ResetProgressBar();
        CalculatePointNeed();
        //CalculateCannonsCount();
        gm.UIManager.ShopPanel.gameObject.SetActive(true);
        gm.ShopManager.ChangeSkinWithoutItem();
        gm.EnergyTotal = 160;
        gm.UIManager.fillBigSlider();
        gm.UIManager.BigEnergySlider.gameObject.SetActive(true);
        isFailed = false;
    }

    public void StartSlide() { StartCoroutine(Slide());}
    public IEnumerator Slide()
    {   
        var lasers = FindObjectsOfType<Laser>();
        foreach(Laser laser in lasers)
        {
            laser.GetComponent<TrailRenderer>().enabled = false;
        }
        hvatitPlz = false;
        isFinished = false;
        gm.SkyBoxManager.ChangeSkybox();
        gm.UIManager.ResetProgressBar();
        gm.UIManager.LvlCountUpdate();
        gm.StopLines();
        gm.UnlockMirrors();

        iTween.MoveAdd(currentObj, Vector3.right * -25, 2f);
        NextObj = Instantiate(Levels[currentlvl].lvlPrefab, Vector3.right * 10, Levels[nextLVL].lvlPrefab.transform.rotation) as GameObject;

        iTween.MoveTo(NextObj, new Vector3(0, 0, 0), 2f);
        PointsCurrent = 0;
        
        
        //CalculateNextPointNeed();
        gm.DisableLasers();
        
        GameObject preObj = currentObj;
        currentObj = NextObj;

        gm.UIManager.ShopPanel.gameObject.SetActive(true);
        gm.ShopManager.ChangeSkinWithoutItem();

        yield return new WaitForSeconds(2);
        DestroyImmediate(preObj.gameObject);
        CalculatePointNeed();
        //CalculateCannonsCount();
        gm.EnableLasers();
        gm.UIManager.BigEnergySlider.gameObject.SetActive(true);
        gm.EnergyTotal = 160;
        gm.UIManager.fillBigSlider();
        ReceiveEnergy = 0;
        Finish = false;
        gm.UIManager.fadeout();
        // gm.UIManager.ShopPanel.gameObject.SetActive(true);
        // gm.ShopManager.ChangeSkinWithoutItem();
    }

    // private void CalculateCannonsCount()
    // {
    //     // int preCannonsCount;
    //     // preCannonsCount = CannonsCount;
    //     // CannonsCount = gm.LaserCount() - preCannonsCount;
    //     Laser[] lasers = FindObjectsOfType<Laser>();
    //     CannonsCount = lasers.Length;
    //     gm.UIManager.SliderCount(CannonsCount);
    // }
    // private void CalculateNextPointNeed()
    // {
    //     int prePointNeed;
    //     prePointNeed = Levels[prevLvl].PointsNeed;
    //     CalculatePointNeed();
    //     Levels[currentlvl].PointsNeed = Levels[currentlvl].PointsNeed - prePointNeed;
    // }
    public void SetCustomLvl()
    {
        gm.UIManager.SetCustomLvL();
        currentlvlUI = currentlvl;
        nextlvlUI = nextLVL;
    }

    public void CalculateLvls()
    {
        if(currentlvl > Levels.Count - 1)
        {
            currentlvl = LvlAfterEnd;
        }

        nextlvlUI = currentlvlUI + 1;
        CountNextLVL = currentlvl + 1;
        if (CountNextLVL < Levels.Count) nextLVL = currentlvl + 1;
        else nextLVL = LvlAfterEnd;

    }

    private void ResetPointsCurrent()
    {
        PointsCurrent = 0;
    }

    private void CalculatePointNeed()
    {
        Levels[currentlvl].PointsNeed = 120;
    }

    public void ResetGame()
    {
        PlayerPrefs.DeleteAll();
    }
}


