﻿using System;
using System.Collections.Generic;
using UnityEngine;

public class SkyBoxManager : MonoBehaviour
{
    public Game_Manager gm;
    public int Interval;
    private int catchlvl;
    private int count = 0;
    public Renderer background;
    public List<Colors> colors = new List<Colors>();
    [Serializable]
    public class Colors
    {
        public Color32 color1;
        public Color32 color2;
    }

    void Start()
    {
        if (!PlayerPrefs.HasKey("Saved SkyCount")) count = 0;
        else count = PlayerPrefs.GetInt("Saved SkyCount");

        background.sharedMaterial.SetColor("_Color" ,colors[count].color1);
        background.sharedMaterial.SetColor("_Color2" ,colors[count].color2);
        catchlvl = PlayerPrefs.GetInt("Saved LVL UI") + Interval - 1;
    }
    public void ChangeSkybox()
    {
        if(gm.LVLManager.currentlvlUI >= catchlvl)
        {
            count++;
            if(count > colors.Count - 1)
            {
                count = 0;
                background.sharedMaterial.SetColor("_Color" ,colors[count].color1);
                background.sharedMaterial.SetColor("_Color2" ,colors[count].color2);
                PlayerPrefs.SetInt("Saved SkyCount", count);
            }
            else
            {
                background.sharedMaterial.SetColor("_Color" ,colors[count].color1);
                background.sharedMaterial.SetColor("_Color2" ,colors[count].color2);
                catchlvl += Interval;
                PlayerPrefs.SetInt("Saved SkyCount", count);
            }
        }
    }

}
