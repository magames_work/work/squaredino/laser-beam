﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using TMPro;

public class Game_Manager : MonoBehaviour
{
    [Header("Scripts")]
    public LVLManager LVLManager;
    public UIManager UIManager;
    public VibroManager VibroManager;
    public SkyBoxManager SkyBoxManager;
    public ShopManager ShopManager;
    [Header("Config")]
    public float FadeOutSpeed;
    public float LaserSpeed;
    public float burstInterval;
    public float EnergyTotal;
    public float LaserDefaultEnergy;
    public float LaserConsumption;
    public float RotationByClick;
    public bool IsSwipeControl;
    private bool isStarted;
    public int startsTotal;
    [HideInInspector]
    public float CalcEnergy;
    [Header("Staff")]
    public Material CannonMaterial1;
    public Material CannonMaterial2;
    public Material LightOff;
    public Material LightOn;
    public GameObject halo;
    public GameObject laserPref;
    private GameObject cannon;


    public bool startBurst;
    private void Awake() {
        Application.targetFrameRate = 50;
    }
    
    Laser laser;
    [HideInInspector]
    public bool fadeout;
    float alpha;
    public GameObject lastLaser;

    private IEnumerator LaserBurst()
    {
        while(true)
        {
            if(startBurst && !LVLManager.Finish && UIManager.BigEnergySlider.value > 0)
            {
                if (!burstEnable)
                {
                    yield return new WaitForSeconds(burstInterval);
                    
                    try{
                        GameObject laserObj = Instantiate(laserPref, cannon.transform) as GameObject;
                        laserObj.GetComponent<Rigidbody>().collisionDetectionMode = CollisionDetectionMode.Discrete;
                        UIManager.EnergyConsumption(LaserDefaultEnergy);
                        StartLine(laserObj.GetComponent<Laser>());
                        VibroManager.TriggerLightImpact();
                    }catch{}

                }
            }

            yield return new WaitForEndOfFrame();
        }
    }

    private IEnumerator TrailControl()
    {
        while(true)
        {
            if(!burstEnable && startBurst && !LVLManager.Finish && UIManager.BigEnergySlider.value > 0)
            {
                if(Input.GetMouseButton(0) & lastLaser != null)
                {
                    lastLaser.GetComponent<TrailRenderer>().time = Time.time - catchTime;
                }
            }

            yield return new WaitForFixedUpdate();
        }
    }

    float catchTime;
    bool burstEnable = true;

    bool isUIHitting;

    void Update() 
    {
        foreach (Touch touch in Input.touches)
        {
            int id = touch.fingerId;
            if (EventSystem.current.IsPointerOverGameObject(id))
            {
                isUIHitting = true;
            }
            else
            {
                isUIHitting = false;
            }
        }

        Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);

        try{
            if(LVLManager.Finish)
                lastLaser.GetComponent<TrailRenderer>().enabled = false;
        }catch{}
        
        
        if(!isUIHitting && UIManager.BigEnergySlider.value > 0 && Physics.Raycast(ray, out hit, 100) & !EventSystem.current.IsPointerOverGameObject())
        {
            if(Input.GetMouseButtonDown(0))
            {
                if(hit.transform.CompareTag("Cannon") | hit.transform.CompareTag("background"))
                {
                    // if(hit.transform.tag == "Cannon")
                    // {
                        cannon = GameObject.FindGameObjectWithTag("Cannon");
                        GameObject laserObj = Instantiate(laserPref, cannon.transform) as GameObject;
                        laserObj.GetComponent<TrailRenderer>().time = 0.05f;
                        laserObj.GetComponent<Laser>().StartEnergy = 0;
                        laserObj.GetComponent<Rigidbody>().collisionDetectionMode = CollisionDetectionMode.Continuous;
                        laserObj.name = "leader";

                        VibroManager.TriggerLightImpact();

                        lastLaser = laserObj;

                        StartLine(lastLaser.GetComponent<Laser>());
                        lastLaser.GetComponent<TrailRenderer>().enabled = true;
                        catchTime = Time.time;
                    //}
                }
            }
        }                    
    }

    RaycastHit hit;
    private void FixedUpdate() 
    {
        Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
        
        if(!isUIHitting && UIManager.BigEnergySlider.value > 0 && Physics.Raycast(ray, out hit, 100) & !EventSystem.current.IsPointerOverGameObject())
        {
            if(burstEnable && Input.GetMouseButton(0))
            {
                if(hit.transform.CompareTag("Cannon") | hit.transform.CompareTag("background"))
                {
                    //if(hit.transform.tag == "Cannon")
                    //{
                        // if(catchTime + 0.065f < Time.time)
                        // {
                            burstEnable = false;
                            startBurst = true;
                            //catchTime = Time.time;
                        //}
                        
                    //}
                }
            }
        }
        if(Input.GetMouseButtonUp(0))
        {
            startBurst = false;
            burstEnable = true;
        }
    }

    void Start()
    {
        StartCoroutine("LaserBurst");
        StartCoroutine(TrailControl());
        alpha = 1;
        if (!PlayerPrefs.HasKey("Stars")) startsTotal = 0;
        else startsTotal = PlayerPrefs.GetInt("Stars");
    }
    public void UnlockMirrors()
    {
        var mirrors = FindObjectsOfType<Mirror>();
        foreach(Mirror mirror in mirrors)
        {
            mirror.lockRotation = false;
        }
    }
    public void LockMirrors()
    {
        var mirrors = FindObjectsOfType<Mirror>();
        foreach(Mirror mirror in mirrors)
        {
            mirror.lockRotation = true;
        }
    }
    public int LaserCount()
    {
        var lasers = FindObjectsOfType<Laser>();
        
        return lasers.Length;
    }
    public void StopLines()
    {
        var lasers = FindObjectsOfType<Laser>();
        foreach(Laser laser in lasers)
        {
            DelLines(laser);
            laser.isStart = false;
        }
    }
    public void StopLine(Laser laser)
    {
        LaserReturnToStartPoint(laser);
        laser.lr.enabled = false;

        laser.isStart = false;
    }
    public void StartLines()
    {
        var lasers = FindObjectsOfType<Laser>();
        foreach(Laser laser in lasers)
        {
            laser.isStart = true;
        }
    }
    public void StartLine(Laser laser)
    {
        LaserReturnToStartPoint(laser);

        laser.isStart = true;
    }
    public void DelLines(Laser laser)
    {
        //laser.lr.positionCount = 0;
    }
    public void LaserReturnToStartPoint(Laser laser)
    {
        laser.GetComponent<Rigidbody>().velocity = Vector3.zero;
        //laser.lr.positionCount = 2;
        laser.transform.position = laser.GetComponentInParent<MeshRenderer>().gameObject.transform.position;
        laser.transform.localRotation = Quaternion.Euler(-90,0,0);
        //laser.lr.SetPosition(0, laser.GetComponentInParent<MeshRenderer>().gameObject.transform.position);
        //laser.lr.SetPosition(1, laser.GetComponentInParent<MeshRenderer>().gameObject.transform.position);
    }

    public void CalculateDifOfEnergy(float StartEnergy, float EndEnegry)
    {
        LVLManager.AmountEnergy += StartEnergy;

        LVLManager.ReceiveEnergy += StartEnergy - (StartEnergy - EndEnegry);


    }
    public void DisableLasers()
    {
        var lasers = FindObjectsOfType<Laser>();
        foreach(Laser laser in lasers)
        {
            laser.transform.parent.gameObject.GetComponent<SphereCollider>().enabled = false;
            laser.enabled = false;
        }
    }
    public void EnableLasers()
    {
        var lasers = FindObjectsOfType<Laser>();
        foreach(Laser laser in lasers)
        {
            laser.transform.parent.gameObject.GetComponent<SphereCollider>().enabled = true;
            laser.enabled = true;
        }
    }
    public void SwipeOrtap()
    {
        if(IsSwipeControl)
        {
            IsSwipeControl = false;
            print("Tipe");

        }
        else if(!IsSwipeControl)
        {
            IsSwipeControl = true;
            print("Swipe");
        }
    }
}
