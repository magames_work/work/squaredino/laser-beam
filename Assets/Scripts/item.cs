﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class item : MonoBehaviour
{
    public Material mat;
    public Material Lasermat;
    public Mesh mesh;
    public int index;
    public int Cost;
    public bool locked;

    private void Start()
    {
        if (locked) { transform.Find("LockPanel").transform.Find("StarCost").transform.Find("CostText").GetComponent<TextMeshProUGUI>().text = Cost.ToString(); }
        if (!locked) { transform.Find("LockPanel").gameObject.SetActive(false); }
    }

    public void Unlock()
    {
        locked = false;
        transform.Find("LockPanel").gameObject.SetActive(false);
    }
}
