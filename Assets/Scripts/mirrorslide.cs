﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class mirrorslide : MonoBehaviour
{
    Rigidbody rb;
    float pitch;
    bool clicked = false;
    private int layerMask = ~(1 << 10);
    public Laser laserCollideWith = null;
    private Game_Manager gm;
    void Start()
    {
        gm = FindObjectOfType<Game_Manager>();
        //rb = GetComponent<Rigidbody>();
    }

    // Update is called once per frame
    Vector3 localhit;
    void Update()
    {
        Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
        RaycastHit hit;
        RaycastHit hit1;
        if(Physics.Raycast(ray, out hit, 100, 1 << 12))
        {
            if(Input.GetMouseButtonDown(0) & hit.transform == transform)
            {
                clicked = true;
                GameObject[] boxes = GameObject.FindGameObjectsWithTag("longplatform");
                foreach(GameObject go in boxes)
                {
                    if(go.transform != transform.parent.transform)
                    {
                        go.GetComponent<BoxCollider>().enabled = false;
                    }
                }
            }
        }
        if(Input.GetMouseButtonUp(0))
        {
            clicked = false;
            GameObject[] boxes = GameObject.FindGameObjectsWithTag("longplatform");
                foreach(GameObject go in boxes)
                {
                    go.GetComponent<BoxCollider>().enabled = true;
                }
        }
        if(Physics.Raycast(ray, out hit1, 100, 1 << 10))
        {
            if(hit1.transform == transform.parent.transform)
            {
                localhit = transform.parent.gameObject.transform.InverseTransformPoint(hit1.point);

                if(clicked == true & laserCollideWith == null)
                {      
                    transform.localPosition = (new Vector3(localhit.x, 110,10));
                }  
                if(clicked == true & laserCollideWith != null)
                {      
                    gm.StopLine(laserCollideWith);
                    transform.localPosition = (new Vector3(localhit.x, transform.localPosition.y,transform.localPosition.z));
                }  
            }
            
        }
    }
}
