﻿using UnityEngine;
using System.Collections;

public class Laser : MonoBehaviour
{
    public float LaserCustomEnergy;
    [HideInInspector]
    public float Energy;
    public float StartEnergy;
    public float EndEnergy;
    private Game_Manager gm;
    private Rigidbody rb;
    [HideInInspector]
    public TrailRenderer lr;
    [HideInInspector]
    public bool isfinished, isStart = false;
    private float maxDist = 200;
    private int maxRefl = 50;
    public bool lockCannon = false;
    public bool clicked = false;
    public RaycastHit hintHit;
    
    void Start()
    {
        gm = FindObjectOfType<Game_Manager>();
        rb = GetComponent<Rigidbody>();
        lr = GetComponent<TrailRenderer>();
        //lr.startWidth = .6f;
        //lr.endWidth = .6f;
        isfinished = false;
        clicked = false;
        rb.freezeRotation = true;
        if(LaserCustomEnergy == 0)
        {
            Energy = gm.LaserDefaultEnergy;
            StartEnergy = Energy;
        }
        else
        {
            Energy = LaserCustomEnergy;
            StartEnergy = Energy;
        }
        StartCoroutine("dest");
        
        
        GetComponent<SphereCollider>().radius = 0.1f;
    }
    
    IEnumerator dest()
    {
        yield return new WaitForSeconds(5);
        if(!hitFinish)
        {
            Destroy(gameObject);
        }
    }

    public Laser currentCannon = null;
    void Update()
    {
        //DrawPredictedReflectionPattern(this.transform.position + transform.up * 0.75f, this.transform.up, maxRefl);
    }

    void FixedUpdate()
    {
        if(!isfinished & isStart & Energy > 0 & !lockCannon & !gm.fadeout)
        {   
            transform.Translate(Vector3.up * Time.deltaTime * gm.LaserSpeed);

        }
        
        if(Energy == 0 & isfinished)
        {
            gm.LVLManager.CannonsOutOfEnergy ++;
            Energy = -1;
        }
        Physics2D.IgnoreLayerCollision(8, 8);
    }

    public bool hitFinish;
    void OnCollisionEnter(Collision collision)
    {
        if(collision.transform.tag == "Mirror")
        {
            collision.transform.GetChild(0).transform.GetComponent<MeshRenderer>().material.color = Color.yellow;
            Vector3 reflectedPosition = Vector3.Reflect(transform.up, collision.contacts[0].normal);
            //transform.up = reflectedPosition * Time.deltaTime * gm.LaserSpeed;
            var direction = transform.up;
            //rb.rotation = Quaternion.Euler(0, 0, Mathf.Atan2(reflectedPosition.y, reflectedPosition.x) * Mathf.Rad2Deg - 90f);
            transform.rotation = Quaternion.Euler(0, 0, Mathf.Atan2(reflectedPosition.y, reflectedPosition.x) * Mathf.Rad2Deg - 90f);
            //rb.MoveRotation(Quaternion.Euler(0, 0, Mathf.Atan2(direction.y, direction.x) * Mathf.Rad2Deg - 90f));
            //lr.positionCount++;
            if(lr.enabled)
                lr.SetPosition(lr.positionCount - 1, transform.position);
            try{
                
                collision.gameObject.GetComponentInParent<Mirror>().laserCollideWith = this;
            }catch{}
        }

        if(collision.transform.tag == "finish")
        {
            //EndEnergy = Energy;
            //gm.CalculateDifOfEnergy(StartEnergy, EndEnergy);
            //isfinished = true;
            if(!gm.LVLManager.Finish)
            {
                rb.velocity = Vector3.zero;
                rb.freezeRotation = true;
                gm.LVLManager.PointsCurrent += StartEnergy;
                gm.UIManager.OnPoint();
                gm.LVLManager.ReceiveEnergy += StartEnergy;
                Energy = 0;
                hitFinish = true;
                this.GetComponent<SphereCollider>().isTrigger = true;
            }
            else
                GetComponent<TrailRenderer>().enabled = false;
            
            //Destroy(gameObject, 0.5f);
        }
        if(collision.transform.tag != "Mirror" & collision.transform.tag != "finish" & collision.transform.tag != "laser")
        {
            lockCannon = true;
            rb.freezeRotation = true;
        }
    }

    public void DrawPredictedReflectionPattern(Vector3 position, Vector3 direction, int reflectionsRemaining)
    {
        if (reflectionsRemaining == 0) {
            return;
        }
        Vector3 startingPosition = position;
        Ray ray = new Ray(position, direction);
        RaycastHit hit;
        if (Physics.Raycast(ray, out hit, maxDist, ~(1 << 8)))
        {
            if(hit.transform.tag == "Mirror")
            {
                direction = Vector3.Reflect(direction, hit.normal);
                position = hit.point; 
            }
            if(hit.transform.tag == "finish")
            {
                direction = Vector3.zero;
                position = hit.point; 
            }
            this.hintHit = hit;
        }
        else
        {
            position += direction * maxDist;
        }
        Debug.DrawLine(startingPosition, position);

        DrawPredictedReflectionPattern(position, direction, reflectionsRemaining - 1);
    }
}
