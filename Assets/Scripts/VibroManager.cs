﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using MoreMountains.NiceVibrations;

public class VibroManager : MonoBehaviour
{
    public bool IsVibroActive {get{ return isVibroActive; } }
    private bool isVibroActive = true;
    public float LaserVibroInterval;
    public Image iconOn;
    public Image IconOff;

    public void VibroState()
    {
        if (IsVibroActive)
        {
            //vibro off
            IconOff.enabled = true;
            iconOn.enabled = false;
            isVibroActive = false;
        }
        else
        {
            //vibor on
            IconOff.enabled = false;
            iconOn.enabled = true;
            MMVibrationManager.Haptic(HapticTypes.Success);
            isVibroActive = true;
        }
    }

    #region Vibrations
    public void TriggerSelection()
    {
        if(isVibroActive)
            MMVibrationManager.Haptic(HapticTypes.Selection);
    }


    public void TriggerSuccess()
    {
        if(isVibroActive)
            MMVibrationManager.Haptic(HapticTypes.Success);
    }

    public void TriggerWarning()
    {
        if(isVibroActive)
            MMVibrationManager.Haptic(HapticTypes.Warning);
    }

    public void TriggerFailure()
    {
        if(isVibroActive)
            MMVibrationManager.Haptic(HapticTypes.Failure);
    }

    public void TriggerLightImpact()
    {
        if(isVibroActive)
            MMVibrationManager.Haptic(HapticTypes.LightImpact);
    }

    public void TriggerMediumImpact()
    {
        if(isVibroActive)
            MMVibrationManager.Haptic(HapticTypes.MediumImpact);
    }

    public void TriggerHeavyImpact()
    {
        if(isVibroActive)
            MMVibrationManager.Haptic(HapticTypes.HeavyImpact);
    }
    #endregion
}


