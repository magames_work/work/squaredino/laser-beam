﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ShopManager : MonoBehaviour
{
    public Game_Manager gm;

    public int CurrentSkin;
    public int availability;

    private void Awake()
    {
        SetItemId();

        if (!PlayerPrefs.HasKey("CurrentSkinId")) { CurrentSkin = 0; PlayerPrefs.SetInt("CurrentSkinId", CurrentSkin); }
        else { CurrentSkin = PlayerPrefs.GetInt("CurrentSkinId");}
        if (!PlayerPrefs.HasKey("CurrentAvailability")) { availability = 1; PlayerPrefs.SetInt("CurrentAvailability", availability); }
        else { availability = PlayerPrefs.GetInt("CurrentAvailability"); }


    }

    public void StartChangeSkin()
    {
        GameObject[] lasers = GameObject.FindGameObjectsWithTag("Cannon");
        item[] items = FindObjectsOfType<item>();

        foreach (GameObject cannon in lasers)
        {
            foreach (item item in items)
            {
                if((availability & 1 << item.index) == 1 << item.index)
                {
                    item.Unlock();
                }
                if (item.index == CurrentSkin)
                { 
                    ChangeSkin(item);
                }
            }
        }
    }
    public void ChangeSkinWithoutItem()
    {
        GameObject[] lasers = GameObject.FindGameObjectsWithTag("Cannon");
        item[] items = FindObjectsOfType<item>();

        foreach (GameObject cannon in lasers)
        {
            foreach (item item in items)
            {
                if (item.index == CurrentSkin)
                {
                    ChangeSkin(item);
                }
            }
        }
    }



    public void Click(item item)
    {
        if ((availability & 1 << item.index) == 1 << item.index)
        {
            print(1 << item.index);
        }
        if (item.locked)
        {
            if (gm.startsTotal >= item.Cost)
            {
                availability += 1 << item.index;
                gm.startsTotal -= item.Cost;
                item.Unlock();
                ChangeSkin(item);
                PlayerPrefs.SetInt("CurrentAvailability", availability);
                PlayerPrefs.SetInt("Stars", gm.startsTotal);
            }
            else
            {
                print("NOT ENOUGH");
            }
        }
        else
        {
            ChangeSkin(item);
        }
    }

    public void ChangeSkin(item item)
    {
        item[] items = FindObjectsOfType<item>();
        foreach(item item_ in items)
        {
            if(item_ == item)
            {
                item_.transform.GetChild(2).gameObject.SetActive(true);
            }
            else
            {
                item_.transform.GetChild(2).gameObject.SetActive(false);
            }
        }
        gm.UIManager.currentSkinSprite.sprite = item.transform.GetChild(0).GetComponent<Image>().sprite;
        GameObject[] lasers = GameObject.FindGameObjectsWithTag("Cannon");
        foreach (GameObject cannon in lasers)
        {
            cannon.transform.localScale = new Vector3(0.01f, 0.01f, 0.008f);
            cannon.transform.GetComponent<MeshFilter>().mesh = item.mesh;
            cannon.transform.GetComponent<MeshRenderer>().material = item.mat;
            cannon.transform.GetComponent<SphereCollider>().radius = 120;
            //cannon.transform.GetComponent<TrailRenderer>().material = item.Lasermat;
            CurrentSkin = item.index;
        }
        PlayerPrefs.SetInt("CurrentSkinId", CurrentSkin);
    }

    private void SetItemId()
    {
        item[] items = FindObjectsOfType<item>();
        for (int i = 0; items.Length > i; i++)
        {
            items[i].index = i;
        }
    }
}
