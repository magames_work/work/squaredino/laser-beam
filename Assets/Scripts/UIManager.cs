﻿using System.Collections;
using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;
using TMPro;

public class UIManager : MonoBehaviour
{
    public Game_Manager gm;
    
    [Header("TextMeshPro")]
    public TextMeshProUGUI NextLVL;
    public TextMeshProUGUI LvlCounter, lvlcleared, starsCount;
    public Text fps;

    [Header("GameObjects")]
    public GameObject instr;
    public GameObject LvlClearedPanel, LvlClearedBack, star1, star2, star3, RestartLvlPanel, restartButton1, restartButton2, nextLvlButton;
    public Image currentSkinSprite;

    [Header("Rect Transform")]
    public RectTransform ShopPanel, shop, Tip, Canvas;

    [Header("Sliders")]
    public Slider progressBar;
    public Slider EnergySlider1;
    public Slider EnergySlider2;
    public Slider BigEnergySlider;

    [Header("Particle System")]
    public ParticleSystem FireworkParticleSystem; 
    public ParticleSystem StarsSparcles;

    private bool IsMenuSwiped;
    [HideInInspector]
    public bool skipCollectStars = false;

    int current;

    private void Start()
    {
        fillBigSlider();
    }

    public void fillBigSlider()
    {
        BigEnergySlider.maxValue = gm.EnergyTotal;
        BigEnergySlider.value = gm.EnergyTotal;
    }

    private void Update() 
    {
        starsCount.text = ("x " + gm.startsTotal);
        current = (int)(1f / Time.unscaledDeltaTime);
        fps.text = current.ToString();

        if (ShopPanel.offsetMin.y == -2000)
           ShopPanel.gameObject.SetActive(false);
        else
           ShopPanel.gameObject.SetActive(true);

    }
    public void EnergyConsumption(float enegry)
    {
        DOTween.To(() => gm.EnergyTotal, x => gm.EnergyTotal = x, gm.EnergyTotal -= enegry, .5f);

        DOTween.To(() => BigEnergySlider.value, x => BigEnergySlider.value = x, gm.EnergyTotal, .5f);
    }
    // public void SliderCount(int lasers)
    // {
    //     BigEnergySlider.maxValue = 0;
    //     var Lasers = FindObjectsOfType<Laser>();
    //     int i = 0;
    //     foreach(Laser laser in Lasers)
    //     {
    //         i++;
    //         if(i == 1)
    //         {
    //             // Vector3 laserPos = laser.transform.position;
    //             // EnergySlider1.transform.position = new Vector3(laserPos.x, laserPos.y, -2);
    //             BigEnergySlider.maxValue += laser.StartEnergy;
    //             BigEnergySlider.value += laser.StartEnergy;
    //             EnergySlider1.maxValue = laser.StartEnergy;
    //             EnergySlider1.value = laser.StartEnergy;
    //         }
    //         if(i == 2)
    //         {
    //             // Vector3 laserPos = Camera.main.WorldToScreenPoint(laser.transform.position);
    //             // EnergySlider2.transform.position = new Vector3(laserPos.x, laserPos.y - 10, laserPos.z);
    //             BigEnergySlider.maxValue += laser.StartEnergy;
    //             BigEnergySlider.value += laser.StartEnergy;
    //             EnergySlider2.maxValue = laser.StartEnergy;
    //             EnergySlider2.value = laser.StartEnergy;
    //         }
    //         laser.index = i;
    //     }
    //     if(lasers < 2)
    //     {
    //         //EnergySlider2.gameObject.SetActive(false);
    //     }
    //     if(lasers >= 2)
    //     {
    //         //EnergySlider2.gameObject.SetActive(true);
    //     }
    // }

    public void LvlCountUpdate()
    {
        LvlCounter.text = "Level: " + gm.LVLManager.currentlvlUI;
        //NextLVL.text = "Level: " + gm.LVLManager.nextlvlUI;
    }
    public void LvlCountUpdateCustom()
    {
        LvlCounter.text = "Level: " + gm.LVLManager.currentlvl;
        //NextLVL.text = "Level: " + gm.LVLManager.nextLVL;
    }

    public void ResetProgressBar()
    {
        DOTween.To(() => progressBar.value, x => progressBar.value = x, 0, .5f);
    }
    float alpha = 0;
    public void OnPoint()
    {
        GameObject finishObj = GameObject.FindGameObjectWithTag("finish");
        gm.halo.SetActive(true);
        gm.halo.transform.position = new Vector3(finishObj.transform.position.x, finishObj.transform.position.y, gm.halo.transform.position.z);

        progressBar.maxValue = gm.LVLManager.Levels[gm.LVLManager.currentlvl].PointsNeed;
        DOTween.To(() => progressBar.value, x => progressBar.value = x, gm.LVLManager.PointsCurrent, 1f);


        DOTween.To(() => alpha, x => alpha = x, alpha + 0.15f, 0f);

        gm.halo.GetComponent<MeshRenderer>().material.DOFade(alpha,1);
    }
    public void fadeout()
    {
        alpha = 0;
        gm.halo.GetComponent<MeshRenderer>().material.DOFade(0,0);
    }
    public void SetCustomLvL()
    {
        LvlCountUpdate();
        ResetProgressBar();
        gm.LVLManager.StartSlide();
    }

    public void LvlCleared()
    {
        StartCoroutine(Lvlcleared());
    }


    Vector3 ScreenPos;
    Vector3 WorldPos;
    IEnumerator Lvlcleared()
    {
        restartButton1.GetComponent<Button>().enabled = false;
        restartButton2.GetComponent<Button>().enabled = false;
        
        FireworkParticleSystem.Play();
        yield return new WaitForSeconds(1.5f);
        LvlClearedBack.GetComponent<Image>().color = new Color32(255, 255, 255, 0);
        LvlClearedPanel.SetActive(true);
        LvlClearedBack.GetComponent<Image>().DOColor(new Color32(255,255,255, 200), 1f);
        restartButton1.SetActive(false);
        restartButton2.SetActive(true);
        restartButton1.GetComponent<Button>().enabled = true;
        BigEnergySlider.gameObject.SetActive(false);
        //lvlcleared.text = "Level Cleared!";
        yield return new WaitForSeconds(1f);

        if(gm.LVLManager.Stars >= 1 & !skipCollectStars)
        {
            yield return new WaitForSeconds(0.25f);
            StarsSparcles.gameObject.SetActive(true);
            StarsSparcles.transform.position = new Vector3(star1.transform.position.x, star1.transform.position.y, -3.5f);
            StarsSparcles.Play();
            yield return new WaitForSeconds(0.25f);
            star1.SetActive(true);
        }
        if(gm.LVLManager.Stars >= 2)
        {
            yield return new WaitForSeconds(0.25f);
            StarsSparcles.transform.position = new Vector3(star2.transform.position.x, star2.transform.position.y, -3.5f);
            StarsSparcles.Play();
            yield return new WaitForSeconds(0.25f);
            star2.SetActive(true);
        }
        if(gm.LVLManager.Stars >= 3)
        {
            yield return new WaitForSeconds(0.25f);
            StarsSparcles.transform.position = new Vector3(star3.transform.position.x, star3.transform.position.y, -3.5f);
            StarsSparcles.Play();
            yield return new WaitForSeconds(0.25f);
            star3.SetActive(true);
        }
        

        yield return new WaitForSeconds(0.5f);
        StarsSparcles.gameObject.SetActive(false);
        nextLvlButton.SetActive(true);
        restartButton1.GetComponent<Button>().enabled = true;
        restartButton2.GetComponent<Button>().enabled = true;
        LvlClearedPanel.SetActive(true);
        //LvlClearedPanel.GetComponent<Button>().enabled = true;
    }
    public void SkipStars()
    {
        skipCollectStars = true;
    }
    public void CollectStars()
    {
        skipCollectStars = false;
    }
    public void LvlFailed()
    {
        RestartLvlPanel.GetComponent<Image>().DOFade(0, 0);
        RestartLvlPanel.SetActive(true);
        RestartLvlPanel.GetComponent<Image>().DOFade(1, 0.2f);
    }

    public bool swapped = true;
    
    private void DragShop()
    {
        if (!swapped)
        {
            shop.GetComponent<Image>().DOFade(0, 1f);
            ShopPanel.DOAnchorPosY(-2000, 1f);
            swapped = true;
        }
        else if (swapped)
        {
            shop.GetComponent<Image>().DOFade(1, 1f);
            ShopPanel.DOAnchorPosY(0, 1f);
            swapped = false;
        }
    }

    //public void LvlEnter()
    //{
    //    if (!IsMenuSwiped)
    //    {
    //        DragButtonsOut();
    //        Tip.GetComponent<Image>().DOFade(0, 1);
    //        IsMenuSwiped = true;
    //    }
    //}

    //public void LvlEscape()
    //{
    //    IsMenuSwiped = false;
    //}
}
