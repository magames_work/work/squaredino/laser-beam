﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Mirror : MonoBehaviour
{
    private Game_Manager gm;
    [HideInInspector]
    public bool lockRotation = false;
    private float addrotate;
    public Laser laserCollideWith = null;
    void Awake()
    {
        gm = FindObjectOfType<Game_Manager>();
        addrotate = gameObject.transform.rotation.eulerAngles.z;
    }
    float startmousepos;
    float mousepos;
    GameObject clickedObj;
    void Update()
    {
        Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
        RaycastHit hit;
        

        if(gm.IsSwipeControl)
        {
            if(Physics.Raycast(ray, out hit, 100))
            {
            }
                if(hit.transform != null)
                {
                    if(hit.transform.GetComponent<Mirror>() == this)
                    {
                        if(Input.GetMouseButtonDown(0))
                        {
                            clickedObj = hit.transform.gameObject;
                            startmousepos = Input.mousePosition.x;
                        }

                    }    

                    mousepos = Input.mousePosition.x;
                }
                if(Input.GetMouseButtonUp(0))
                {
                    if(startmousepos - 70 > mousepos)
                    {
                        if(!lockRotation & laserCollideWith == null & clickedObj != null)
                        {
                            if(addrotate >= 360)
                            {
                                addrotate = 0;
                            }
                            iTween.RotateTo(clickedObj, new Vector3(clickedObj.transform.rotation.eulerAngles.x, gameObject.transform.rotation.eulerAngles.y, addrotate -= gm.RotationByClick), 0.3f);
                        }
                        if(!lockRotation & laserCollideWith != null & clickedObj != null)
                        {
                            gm.StopLine(laserCollideWith);
                            if(addrotate >= 360)
                            {
                                addrotate = 0;
                            }
                            iTween.RotateTo(clickedObj, new Vector3(clickedObj.transform.rotation.eulerAngles.x, gameObject.transform.rotation.eulerAngles.y, addrotate -= gm.RotationByClick), 0.3f);
                        }
                    }

                    if(startmousepos + 70 < mousepos)
                    {
                        if(!lockRotation & laserCollideWith == null & clickedObj != null)
                        {
                            if(addrotate <= 0)
                            {
                                addrotate = 360;
                            }
                            if(addrotate <= -360)
                            {
                                addrotate = 0;
                            }
                            iTween.RotateTo(clickedObj, new Vector3(clickedObj.transform.rotation.eulerAngles.x, gameObject.transform.rotation.eulerAngles.y, addrotate += gm.RotationByClick), 0.3f);
                        }
                        if(!lockRotation & laserCollideWith != null & clickedObj != null)
                        {
                            gm.StopLine(laserCollideWith);
                            
                            if(addrotate <= 0)
                            {
                                addrotate = 360;
                            }
                            if(addrotate <= -360)
                            {
                                addrotate = 0;
                            }
                            iTween.RotateTo(clickedObj, new Vector3(clickedObj.transform.rotation.eulerAngles.x, gameObject.transform.rotation.eulerAngles.y, addrotate += gm.RotationByClick), 0.3f);
                        }
                    }
                    clickedObj = null;
                    startmousepos = mousepos;
                }
            
        }
        
        if(!gm.IsSwipeControl)
        {
            if(Physics.Raycast(ray, out hit, 100, 1 << 11))
            {
                if(hit.transform != null)
                {
                    if(hit.transform.GetComponent<Mirror>() == this)
                    {
                        if(Input.GetMouseButtonUp(0))
                        {
                            if(!lockRotation & laserCollideWith == null)
                            {
                                if(addrotate >= 360)
                                {
                                    addrotate = 0;
                                }
                                if(addrotate <= -360)
                                {
                                    addrotate = 0;
                                }
                                iTween.RotateTo(gameObject, new Vector3(gameObject.transform.rotation.eulerAngles.x, gameObject.transform.rotation.eulerAngles.y, addrotate += gm.RotationByClick), 0.3f);
                            }
                            if(!lockRotation & laserCollideWith != null)
                            {
                                gm.StopLine(laserCollideWith);
                                if(addrotate >= 360)
                                {
                                    addrotate = 0;
                                }
                                if(addrotate <= -360)
                                {
                                    addrotate = 0;
                                }
                                iTween.RotateTo(gameObject, new Vector3(gameObject.transform.rotation.eulerAngles.x, gameObject.transform.rotation.eulerAngles.y, addrotate += gm.RotationByClick), 0.3f);
                            }
                        }
                        clickedObj = null;
                        startmousepos = mousepos;
                    }
                }
            }
        }
        
        
    }
}
